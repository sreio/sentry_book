### 1. Alpine Linux
Alpine Linux 是基于 musl libc 和 busybox 的面向安全的轻量级 Linux 发行版。Alpine 的体积非常小，Alpine 的 Docker 镜像大小仅 5 MB 左右。Alpine 功能比 buysbox 完善，还提供了软件包管理工具 apk (Alpine Package Keeper)。

相关网站:

Alpine Linux 官网: https://www.alpinelinux.org/

Alpine Wiki: https://wiki.alpinelinux.org/

Alpine Package Keeper (apk): https://docs.alpinelinux.org/user-handbook/0.1a/Working/apk.html

Alpine apk 软件包搜索: https://pkgs.alpinelinux.org/

busybox: https://busybox.net/

其他 Linux 软件包管理工具：

apt (Debian/Ubuntu)
yum/dnf (Fedora/CentOS/RedHat)
snap (All Linux)
后面示例以 Alpine 3.13.5 为例。

### 2. 更新/安装/卸载 软件包
#### 更新
```bash
apk update              # 根据远程镜像源更新本地仓库中的所有软件包索引（通常在更新/安装软件包前先更新索引）
apk upgrade             # 从仓库中安装所有可用的软件包升级（升级本地已安装的软件包及其依赖项）

apk upgrade <package>   # 更新指定的软件包
# update 只更新索引, upgrade 才真正更新
```

#### 安装
```bash
apk add <package>               # 安装软件包（并自动安装依赖项）
apk add <package>=<version>     # 安装指定版本软件包

apk --no-cache add <package>    # 安装软件包, 不使用缓存

apk fix <package>               # 在不修改 WORLD 的情况下修复, 重新安装或升级软件包

apk 命令格式: apk [<OPTIONS>...] COMMAND [<ARGUMENTS>...]，
其中每个子命令的 [OPTIONS...] 参数信息可以使用 --help 查询，如: apk add --help
```

#### 卸载
```bash
apk del <package>       # 删除软件包, 如果其依赖项不再本需要, 则将其一起卸载

# 删除软件包时默认会执行类似 apt autoremove 的清理操作
```

### 3. 查询/搜索/缓存 软件包
#### 列出软件包 (apk list)
```bash
apk list [<OPTIONS>...] PATTERN...  # 列出给定模式的软件包

# apk list --help 命令参数 (List options):
#     -I, --installed       只显示已安装的软件包
#     -O, --orphaned        只显示孤立的软件包
#     -a, --available       只显示可用的软件包
#     -u, --upgradable      只显示可升级的软件包
#     -o, --origin          按来源列出软件包
#     -d, --depends         按依赖关系列出软件包
#     -P, --providers       按提供者列出软件包

apk -I list                 # 列出已安装的所有软件包
apk -u list                 # 列出可升级的所有软件包
apk list <package>          # 列出指定软件包
```

#### 查询软件包的详细信息
```bash
apk info <package>          # 列出给定软件包或仓库的详细信息
apk info -R <package>       # 列出给定软件包依赖项
apk info --help             # 查询 apk info 命令的帮助/参数信息

apk dot --installed         # 将已安装软件包的依赖项呈现为 graphviz 图形
apk dot <package>           # 将给定装软件包的依赖项呈现为 graphviz 图形
```

#### 搜索软件包
apk search <string>         # 搜索软件包

#### 缓存管理
apk cache clean         # 删除旧的软件包
apk cache download      # 下载缺少的软件包
apk cache sync          # 删除旧软件包并下载缺少的程序包（上面两个步骤合并为一个）

使用缓存需要先开启缓存: https://wiki.alpinelinux.org/wiki/Local_APK_cache

### 4. 下载/离线安装 apk 软件包
使用 apk fetch 命令下载软件包到本地 (.apk 文件):
```bash

apk fetch [<OPTIONS>...] PACKAGES...    # 从配置的软件包仓库中下载软件包到本地

# apk fetch --help 命令参数 (Fetch options):
#     -L, --link            如果可能的话, 创建硬链接
#     -o, --output DIR      下载到指定文件夹（默认为当前文件夹）
#     -R, --recursive       下载软件包及其依赖项
#     -s, --stdout          将 .apk 文件转储到标准输出 
#     --simulate            模拟请求的操作，不做任何更改
```

下载软件包及其依赖项并离线安装示例:

```bash
$ apk fetch -R <package>            # 下载软件包及其依赖项
$
$ ls                                # 下载的软件包是 .apk 格式
xxxx.apk.   xxxx.apk    ...
$
$ apk add xxxx.apk xxxx.apk ...     # 离线安装 apk 软件包及其依赖项
```

### 5. apk --help
```bash
$ apk --help

apk-tools 2.12.5, compiled for x86_64.

usage: apk [<OPTIONS>...] COMMAND [<ARGUMENTS>...]

Package installation and removal:
  add        Add packages to WORLD and commit changes
  del        Remove packages from WORLD and commit changes

System maintenance:
  fix        Fix, reinstall or upgrade packages without modifying WORLD
  update     Update repository indexes
  upgrade    Install upgrades available from repositories
  cache      Manage the local package cache

Querying package information:
  info       Give detailed information about packages or repositories
  list       List packages matching a pattern or other criteria
  dot        Render dependencies as graphviz graphs
  policy     Show repository policy for packages

Repository maintenance:
  index      Create repository index file from packages
  fetch      Download packages from global repositories to a local directory
  manifest   Show checksums of package contents
  verify     Verify package integrity and signature

Miscellaneous:
  audit      Audit system for changes
  stats      Show statistics about repositories and installations
  version    Compare package versions or perform tests on version strings

This apk has coffee making abilities.
For more information: man 8 apk
```
